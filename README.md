<p align="center">
    <img src="https://gitee.com/Kindear/BlogAssets/raw/master/cnblogs/20200415203158.png"/>


![](https://img.shields.io/badge/build-passing-brightgreen)    ![](https://img.shields.io/badge/progress-70%25-blue)    ![](https://img.shields.io/badge/维护状态-不定期维护-cyan)



####  Version

| 版本      | 开源                                                         | 特性                                                         | 备注                                                         |
| --------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| main      | [main](https://gitee.com/Kindear/mvote)                      | 小程序端仅提供参与信息上传，投票，查看排名功能，内容审核由后端`CMS`进行管理 | 当前分支版本                                                 |
| 极目      | [极目](https://gitee.com/Kindear/mvote/tree/%E6%9E%81%E7%9B%AE%E5%81%A5%E5%BA%B7%E6%8A%95%E7%A5%A8/) | 小程序端仅提供投票，查看排名功能，参与人员由后台`CMS`管理上传，活动，轮播，分组，审核也由`CMS`管理。 | 极目分支分版本(为湖北省楚天都市报提供十万PV稳定服务)         |
| V2-Stable | [Stable](https://gitee.com/Kindear/mvote/tree/StableV2.0/)   | 无`CMS`管理后台，审核功能需要配置管理员ID(不推荐,但是很稳定) | 标签V2 版本                                                  |
| WxH5      | 闭源                                                         | 微信公众号H5投票，与极目分支特性一致，与小程序端数据互通，可由`CMS`管理。**禁止未关注公众号用户投票**，可用于公众号引流。 | 使用微信打开`http://tp.ctmingyi.com`体验（不定时关闭）。配置验收成功后结算，联系方式在最下方。 |



#### 版本特性

- [x] 【重要】新增`CMS`内容管理后台，可以对信息更加方便管理
- [x] 【重要】适配了接口`getUserProfile`

- [x] 将大部分功能用云函数进行了重写
- [x] 项目代码可读性大大增加
- [x] 更改了不规范的命名



#### 部署

> 修改 `config`配置文件

修改`CloudID`为自己的云环境ID

```js
module.exports = {
    CloudID:''
}
```

> 开通云环境`CMS`

在云开发管理后台开通`CMS`内容管理，并创建应用，导入`lib`目录下的配置文件，导入后模式如下。

![image-20211022162018738](README.assets/image-20211022162018738.png)

> 上传并部署云函数

选中云函数，右键 -> 上传并云端安装依赖

> 构建 `WebHook` 

在云开发`CMS`中新增如下`webhook`

![image-20211022162221963](README.assets/image-20211022162221963.png)

> 部署云函数

选择云函数 -> 上传并云端安装依赖



部署有问题请在 `issue`中提出，或联系kindear@foxmail.com



#### CMS后台预览(部分)

![image-20211022162816750](README.assets/image-20211022162816750.png)



![image-20211022162827187](README.assets/image-20211022162827187.png)



#### 存在问题

项目重构比较仓促，时间不够，可能存在些许`BUG`，请各位开发者多多包涵。



#### AD

小程序 , Web , APP 开发可联系   kindear@foxmail.com

