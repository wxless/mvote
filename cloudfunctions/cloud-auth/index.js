// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database();
const BASE = 'cloud-user-base';
const PART = 'cloud-participant';
const _ = db.command;
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  /**获取OPENID并查询是否存在对应用户信息 */
  let openid = wxContext.OPENID;
  /**获取数据库中已存在信息 */
  let baseRes = await db.collection(BASE).where({
    openid
  }).get();
  /**是否可以获取到基础信息？ */
  let selfInfo = {};
  if(baseRes.data.length != 0){
    /**取第一个进行赋值操作 */
    selfInfo = baseRes.data[0];
  }
  /**获取微信开放信息 */
  let userDataRes = await cloud.getOpenData({
    list: [event.cloudID]
  })
  let userData = userDataRes.list[0].data;
  /**插入openid */
  userData.openid = openid;
  /**覆盖并组装 */
  selfInfo = Object.assign(selfInfo, userData);
  console.log(selfInfo)
  /**设置userId */
  let userId = ''
  /**新增 | 更新 */
  if(selfInfo._id == undefined){
    let ans = await db.collection(BASE).add({
      data:selfInfo
    })
    userId =  ans._id;
  }else{
    let docid = selfInfo._id;
    delete selfInfo._id;
    await db.collection(BASE).doc(docid).update({
      data:selfInfo
    })
    userId =  docid;
  }
  /**统计该openid 有多少上传记录 */
  let countRes = await db.collection(PART).where({
    openid
  }).count();
  let count = countRes.total;
  return {
    userId,
    count
  }
}