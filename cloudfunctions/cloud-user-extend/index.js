const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database();
const GROUP = 'cloud-activities-groups';
const BASE = 'cloud-user-base';
const PART = 'cloud-participant';
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  let openid = event.openid;
  /**获取用户表信息并且更新 */
  let userRes = await db.collection(BASE).where({
    openid
  }).get();
  if (userRes.data.length == 0) {
    return {
      errCode: -1,
      errMsg: '没有对应用户信息'
    }
  }
  let docId = userRes.data[0]._id;
  let updateRes = await db.collection(BASE).doc(docId).update({
    data: {
      realName: event.realName,
      mobile: event.mobile
    }
  })
  return {
    errCode:0,
    docId
  }
}