const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database();
const _ = db.command;
const GROUP = 'cloud-activities-groups';
const BASE = 'cloud-user-base';
const PART = 'cloud-participant';
const ACT = 'cloud-activities';

// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()
    // console.log(event)
    let docId = event.actionFilter._id;
    /**获取参与信息 */
    let partRes = await db.collection(PART).doc(docId).get();
    console.log(partRes)
    let partData = partRes.data;
    if(partData.status == 1){
        /**修改为审核通过状态 */

        /**分组参与人数 + 1 */
        await db.collection(GROUP).doc(partData.groupId).update({
            data:{
                partNum:_.inc(1)
            }
        })
        /**总体参加人数 + 1 */
        await db.collection(ACT).doc(partData.activityId).update({
            data:{
                partNum:_.inc(1)
            }
        })
    }else if(partData.status == 0){
        /**分组参与人数 - 1 */
        await db.collection(GROUP).doc(partData.groupId).update({
            data:{
                partNum:_.inc(-1)
            }
        })
        /**总体参加人数 - 1 */
        await db.collection(ACT).doc(partData.activityId).update({
            data:{
                partNum:_.inc(-1)
            }
        })
    }
}