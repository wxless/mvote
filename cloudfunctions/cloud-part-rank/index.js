const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const PART = 'cloud-participant';
const GROUP = 'cloud-activities-groups';
const BASE = 'cloud-user-base';
const db = cloud.database();

const _ = db.command;

// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()
    let activityId = event.activityId;
    let rankRes =  await db.collection(PART).where({
        activityId,
        status:1
    }).orderBy('voteNum','desc').limit(10).get();
    /**获取全部排名数据 */
    let rankData = rankRes.data;
    let userIdSet = new Set();
    rankData.forEach(function(ele,index){
      userIdSet.add(ele.userId)
    })
    let userIdList = Array.from(userIdSet);
    /**查询全部用户信息 */
    let userRes = await db.collection(BASE).where({
      _id:_.in(userIdList)
    }).get();
    let userData = userRes.data;
    /**返回数据 */
    let resList = []
    for(var i=0;i<rankData.length;i++){
      let obj = rankData[i]
      for(var j=0;j<userData.length;j++){
        if(obj.userId == userData[j]._id){
          obj.userInfo = userData[j]
          break
        }
        
      }
      resList.push(obj)
    }
    return resList;
}