// pages/part/index.js
const app = getApp();
const dayjs = require('dayjs');
const cwx = require('profunc.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        realName: '',
        mobile: '',
        userIntro: '',
        groups: [],
        selfInfo: null,
        pickerIndex: 0,
        //图片列表
        imgList: [],
        //视频列表
        videoList: [],
        cloudVideoList: []
    },
    /**获取用户手机号码 */
    getPhoneNumber(e){
        var that = this;
        console.log(e)
        let cloudID = e.detail.cloudID;
        wx.showLoading({
          title: '获取手机号',
        })
        wx.cloud.callFunction({
            name:'openapi',
            data:{
                action:'getPhoneNum',
                CloudID:cloudID
            },
            success:res=>{
                that.setData({
                    mobile:res.result.list[0].data.purePhoneNumber
                })
            },
            complete:res=>{
                console.log(res)
                wx.hideLoading();
            }
        })
    },
    /**触发上传事件 */
    tapPart() {
        var that = this;
        let imgList = this.data.imgList;
        var promiseTasks = []
        for (var i = 0; i < imgList.length; i++) {
            promiseTasks.push(cwx.CloudUploadImage(imgList[i]))
        }
        wx.showLoading({
            title: '上传中~'
        })
        Promise.all(promiseTasks).then(res => {

            // console.log(res)
            let cloudImgList = res 
            let fileList = []
            cloudImgList.forEach(function(ele,index){
                fileList.push(ele.fileID)
            })
            let params = that.data;
            /**执行上传操作 */
            wx.cloud.callFunction({
                name:'cloud-participant-api',
                data:{
                    action:'saveOne',
                    realName:params.realName,
                    mobile:params.mobile,
                    groupId:params.groups[params.pickerIndex]._id,
                    userIntro:params.userIntro,
                    imageList:fileList,
                    videoList:params.cloudVideoList
                },
                success:ans=>{
                    if(ans.result.errCode!=-1){
                        that.setData({
                            succMsg:'上传成功'
                        },()=>{
                            setTimeout(function(){
                                wx.navigateBack({
                                  delta: 0,
                                })
                            },1000)
                        })
                    }else{
                        that.setData({
                            errMsg:ans.result.errMsg
                        })
                    }
                    
                },
                complete:ans=>{
                    console.log(ans)
                    wx.hideLoading();
                }
            })
            //具体处理写在如下
        })
    },
    /**视频切换 */
    swiperchange(e) {
        //console.log(e)
        var id = e.detail.current;
        //这是正在播放的视频组件
        var tlist = this.data.videoList;
        for (var i = 0; i < tlist.length; i++) {
            if (id != i) {
                tlist[i].mvId.pause();
            } else {
                tlist[i].mvId.play();
            }
        }

    },
    /**初始化构建 */
    initVideo() {
        var that = this;
        var vlist = this.data.cloudVideoList;
        var tmpList = [];
        for (var i = 0; i < vlist.length; i++) {

            var obj = {};
            obj.url = vlist[i];
            obj.mvId = wx.createVideoContext('video' + i)
            tmpList.push(obj)
        }
        //console.log(tmplist)
        that.setData({
            videoList: tmpList
        })
    },
    /**视频上传 */
    upvideo() {
        var that = this;
        wx.chooseVideo({
            sourceType: ['album'],
            maxDuration: 60,
            camera: 'back',
            success: res => {
                console.log(res)
                var timeStamp = dayjs().valueOf()
                wx.showLoading({
                    title: '上传~'
                })
                wx.cloud.uploadFile({
                    cloudPath: 'video/' + timeStamp + '.mp4',
                    filePath: res.tempFilePath,
                    success: ans => {
                        console.log(ans)
                        that.setData({
                            cloudVideoList: that.data.cloudVideoList.concat(ans.fileID)
                        }, () => {
                            wx.hideLoading();
                            that.initVideo();
                        })
                    },
                    fail: ans => {
                        console.log(ans)
                    }
                })

            }
        })
    },
    /**选择器修改 */
    PickerChange(e) {
        console.log(e);
        this.setData({
            pickerIndex: e.detail.value
        })
    },
    /**图片选择管理 */
    ChooseImage() {
        wx.chooseImage({
            count: 9, //默认9
            sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album'], //从相册选择
            success: (res) => {
                //console.log(res)
                if (this.data.imgList.length != 0) {
                    this.setData({
                        imgList: this.data.imgList.concat(res.tempFilePaths)
                    })
                } else {
                    this.setData({
                        imgList: res.tempFilePaths
                    })
                }
            }
        });
    },
    ViewImage(e) {
        wx.previewImage({
            urls: this.data.imgList,
            current: e.currentTarget.dataset.url
        });
    },
    DelImg(e) {
        this.data.imgList.splice(e.currentTarget.dataset.index, 1);
        this.setData({
            imgList: this.data.imgList
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        /**参与活动配置信息 */
        var that = this;
        wx.cloud.callFunction({
            name: 'cloud-part-init',
            success: res => {
                that.setData({
                    groups: res.result.groups,
                    selfInfo: res.result.selfInfo
                })
            },
            complete: res => {
                console.log(res)
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})