const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo:null,
        activity: null,
        groupList: [],
        swiperList: [],
        listData:[],
        TabCur: 0,
        scrollLeft: 0,
        type:'',//所选分组
        key: ''
    },
    getProfile(){
        var that = this;
        wx.getUserProfile({
            desc: '获取头像昵称信息',
            success:r=>{
              //   console.log(r)
                let cloudID = r.cloudID;
                wx.showLoading({
                  title: '加载中',
                })
                wx.cloud.callFunction({
                    name:'cloud-auth',
                    data:{
                        cloudID
                    },
                    success:res=>{
                        /**携带用户userId 跳转 */
                        let userId = res.result.userId;
                        // wx.navigateTo({
                        //   url: '/pages/part/index?userId='+userId,
                        // })
                        // that.reqList();
                        that.setData({
                            userInfo:true
                        })
                    },
                    complete:res=>{
                        console.log(res)
                        wx.hideLoading();
                    }
                })
            }
          })
    },
    /**
     * 获取用户信息
     */
    getUserProfile(){
        wx.getUserProfile({
          desc: '获取头像昵称信息',
          success:r=>{
            //   console.log(r)
              let cloudID = r.cloudID;
              wx.showLoading({
                title: '加载中',
              })
              wx.cloud.callFunction({
                  name:'cloud-auth',
                  data:{
                      cloudID
                  },
                  success:res=>{
                      /**携带用户userId 跳转 */
                      let userId = res.result.userId;
                      wx.navigateTo({
                        url: '/pages/part/index?userId='+userId,
                      })
                  },
                  complete:res=>{
                      console.log(res)
                      wx.hideLoading();
                  }
              })
          }
        })
    },
    /**请求列表 */
    reqList(){
        var that = this;
        let listData = that.data.listData
        let step = listData.length;
        let groupId = that.data.groupList[that.data.TabCur]._id;
        wx.showLoading({
          title: '加载~',
        })
        wx.cloud.callFunction({
            name:'cloud-participant-api',
            data:{
                action:'getList',
                step,
                groupId
            },
            success:res=>{
                if(res.result.errCode == -1){
                    wx.showToast({
                        title: res.result.errMsg,
                        icon:'none'
                      })
                }else{
                    that.setData({
                        listData:listData.concat(res.result.data)
                    })
                }
            },
            complete:res=>{
                console.log(res.result)
                wx.hideLoading();
            }
        })
    },
    /**
     * 切换TAB
     * @param {*} e 
     */
    tabSelect(e) {
        var that = this;
        var type = this.data.groupList[e.currentTarget.dataset.id].groupName;
        console.log(type)
        that.setData({
            TabCur: e.currentTarget.dataset.id,
            scrollLeft: (e.currentTarget.dataset.id - 1) * 60,
            type: type,
            listData: []
        }, () => {
           that.reqList();
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        app.emitter.on('getUserProfile',res=>{
            this.getUserProfile();
        })
        /**获取全局配置 */
        var that = this;
        wx.showLoading({
            title: '加载中',
        })
        wx.cloud.callFunction({
            name: 'cloud-config',
            success: res => {
                that.setData({
                    activity: res.result.activity,
                    groupList: res.result.groupList,
                    swiperList: res.result.swiperList,
                    userInfo:res.result.userInfo || null
                }, () => {
                  
                    /**配置活动信息缓存 */
                    wx.setStorageSync('activity', res.result.activity)

                    wx.setStorageSync('swiperList', res.result.swiperList)
                    that.reqList();
                })
            },
            complete: res => {
                console.log(res.result)
                wx.hideLoading();
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})