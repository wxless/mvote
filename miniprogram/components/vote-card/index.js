// components/vote-card/index.js
const app = getApp();
Component({
    options: {
        addGlobalClass: true,
        multipleSlots: true
    },
    /**
     * 组件的属性列表
     */
    properties: {
        item: {
            type: Object,
            observer:function(newVal,oldVal){
                var that = this;
                /**监听信息变化 */
                if(newVal._id!=undefined){
                    wx.cloud.callFunction({
                        name:'cloud-vote-status',
                        data:{
                            partId:newVal._id
                        },
                        success:res=>{
                            // if(res.result.errMsg == "基础用户信息不存在"){
                            //     app.emitter.emit('getUserProfile')
                            // }
                            that.setData({
                                canVote:res.result.canVote,
                                voteList:res.result.voteList
                            })
                        },
                        complete:res=>{
                            console.log(res)
                        }
                    })
                }
            }
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        canVote:false,
        voteList:[]
    },

    /**
     * 组件的方法列表
     */
    methods: {
        /**点击跳转 */
        tapDetail(e){
            wx.navigateTo({
              url: '/pages/detail/index?id='+this.properties.item._id,
            })
        },
        /**点击投票 */
        tapVote(e){
            var that = this;
            
            console.log(e)
            // console.log(this.properties.item)
            wx.showLoading({
                title:'投票~'
            })
            wx.cloud.callFunction({
                name:'cloud-vote',
                data:{
                    partId:this.properties.item._id
                },
                success:res=>{
                    if(res.result.errCode == -1){
                        wx.showToast({
                          title: res.result.errMsg,
                          icon:'none'
                        })
                        if(res.result.errMsg=='基础用户信息不存在'){
                            app.emitter.emit('getUserProfile')
                        }
                    }else{
                        that.setData({
                            canVote:false
                        })
                    }
                },
                
                complete:res=>{
                    wx.hideLoading();
                    console.log(res)
                }
            })
        }
    }
})