<p align="center">
    <img src="https://gitee.com/Kindear/BlogAssets/raw/master/cnblogs/20200415203158.png"/>


![](https://img.shields.io/badge/build-passing-brightgreen)    ![](https://img.shields.io/badge/progress-90%25-blue)    ![](https://img.shields.io/badge/维护状态-不定期维护-cyan)



- [x] 活动发布
- [x] 活动审核
- [x] 图片/视频上传
- [x] 多形式匹配

#### 介绍

项目基于微信云开发生态构建 -- 投票小程序

#### 软件架构

1. 客户端小程序

   基于微信云开发构建投票小程序

2. `Web`管理端

   基于微信云开发内容管理 -- 构建一个简单的内容管理后台

#### BUG 

`bug`请在 `issue`中提出，本次测试并未收敛



#### 部署安装

1. [小程序](deploy/applet.md "小程序部署")
2. [`Web`管理端](deploy/web-admin.md "Web管理")

部署有问题请在 `issue`中提出，或联系kindear@foxmail.com

